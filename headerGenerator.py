import csv
import os
import sys

nameKey = "Name"

inputFile = sys.argv[1]


sys.stdout.write("Time")
with open(inputFile, 'rb') as inFile:
        reader = csv.DictReader(inFile, delimiter = ',')
        for row in reader:
                sys.stdout.write(","+row.get(nameKey))
