'''
Luis Jira 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script collects localCost, globalCost, unfairness (all with Stddev) and all dimensions of the globalResponse.

Usage:
    copy this file into the root of the EPOS git repository and execute:
        python EPOSdataGetter.py
'''

import csv
import os

data = []

fieldnames = [
    'alpha',
    'beta',
    'globalCost',
    'globalCostStdev',
    'localCost',
    'localCostStdev',
    'unfairness',
    'unfairnessStdev',
    'dim-0',
    'dim-1',
    'dim-2',
    'dim-3',
    'dim-4',
    'dim-5',
    'dim-6',
    'dim-7',
    'dim-8',
    'dim-9',
    'dim-10',
    'dim-11',
    'dim-12',
    'dim-13',
    'dim-14',
    'dim-15',
    'dim-16',
    'dim-17',
    'dim-18',
    'dim-19',
    'dim-20',
    'dim-21',
    'dim-22',
    'dim-23',
    'dim-24',
    'dim-25',
    'dim-26',
    'dim-27',
    'dim-28',
    'dim-29',
    'dim-30',
    'dim-31',
    'dim-32',
    'dim-33',
    'dim-34',
    'dim-35'
]

dirpath = os.getcwd()
print("Base directory (searching for '/output' here): "+dirpath)
walked = os.walk(dirpath+"/output").next()
dirs = walked[1]
for dir in dirs:
    print("dir: "+dir)
    localCostFile = open(os.path.join(dirpath+"/output/"+dir+"/local-cost.csv"), 'rU')
    globalCostFile = open(os.path.join(dirpath+"/output/"+dir+"/global-cost.csv"), 'rU')
    globalResponseFile = open(os.path.join(dirpath+"/output/"+dir+"/global-response.csv"), 'rU')
    unfairnessFile = open(os.path.join(dirpath+"/output/"+dir+"/unfairness.csv"), 'rU')
    confFile = open(os.path.join(dirpath+"/output/"+dir+"/used_conf.txt"), 'rU')
    temp = {}

    reader = csv.DictReader(unfairnessFile, delimiter = ',')
    for i in range(0,39):
        reader.next()
    line = reader.next()
    temp['unfairness']=line['Mean']
    temp['unfairnessStdev']=line['Stdev']

    reader = csv.DictReader(localCostFile, delimiter = ',')
    for i in range(0,39):
        reader.next()
    line = reader.next()
    temp['localCost']=line['Mean']
    temp['localCostStdev']=line['Stdev']

    reader = csv.DictReader(globalCostFile, delimiter = ',')
    for i in range(0,39):
        reader.next()
    line = reader.next()
    temp['globalCost']=line['Mean']
    temp['globalCostStdev']=line['Stdev']

    reader = csv.DictReader(globalResponseFile, delimiter = ',')
    for i in range(0,399):
        reader.next()
    line = reader.next()
    for i in range(0,36):
        temp['dim-'+str(i)] = line['dim-'+str(i)]

    confLines = confFile.readlines()
    splitLines = []
    for line in confLines:
        splitLines.append(line.split(' = '))

    for line in splitLines:
        if line[0]=='alpha':
            temp['alpha'] = float(line[1])
        if line[0]=='beta':
            temp['beta'] = float(line[1])
            break

    data.append(temp)

with open(os.path.join(dirpath+"/dataOutput.csv"), 'w') as out:
    writer = csv.DictWriter(out, fieldnames)
    writer.writeheader()
    for i in range(0, len(data)):
        writer.writerow(data[i])
    print("Wrote to file: ./dataOutput.csv")
