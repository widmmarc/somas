'''
Marc Widmer 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script automates the excecution of all scripts relevant to the dataset generation.

Usage:
    Change the time point below to specify which datasets should be generated.
'''

from subprocess import call
import datetime
import os, sys

#very ugly but easier than goingback and making everything consistent
date = "15-11-2018"
date2 = "2018-11-15"

#These two timepoints determine the datasets created. Must be of the format xx:x0.
time = [03, 50]
endTime = [23, 50]

answer = "N"

print("Hi! If you continue this program, the old data will be overwritten.\nYou can back them up now.")
while answer != "Y":
    answer = raw_input("Continue with Y: ")

print("Clearing old data...")
currDir = os.path.dirname(os.path.abspath((__file__)))
agentsPath = os.path.join(currDir, "randAgentPositions")
for file in os.listdir(agentsPath):
    filePath = os.path.join(agentsPath, file)
    if os.path.isfile(filePath):
        os.unlink(filePath)

print("Creating datasets and graphics...")

while time[0]*100+time[1] < endTime[0]*100+endTime[1]:
    if time[1] == 50:
        time[1] = 0
        time[0] += 1
    else:
        time[1] += 10

    timeEnc = datetime.time(time[0], time[1])
    timeString = timeEnc.strftime("%H:%M")

    print("\nProcessing %s" % timeString)

    print("Creating agents for %s" % timeString)
    call(["python", "agentsCreator.py", "%s.csv" % date, "parkingOptions.csv", date2, timeString])

    print("Creating agent plans for %s" % timeString)
    call(["python", "plansCreator.py", "randAgentPositions/rand-%s_%s.csv" % (date2, timeString), "parkingOptions.csv", "%s.csv" % date, timeString])

    print("Creating graphics for %s" % timeString)
    call(["python", "mapDrawer.py", "randAgentPositions/rand-%s_%s.csv" % (date2, timeString), "parkingOptions.csv", timeString])

    print("Finished processing %s" % timeString)

print("\nFinished creating datasets and graphics.")

