'''
Yannick Hauri 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script converts the coordinates in the old_parkingOptions.csv file
to simpler coordinates which will be used by all other scripts.

For this script to work, the original file must be named old_parkingOptions.csv.
'''

import os

def fx(x):
    return (x-47.312436)*(10000/0.12938)
def fy(y):
    return ((y-8.412717)/2)*(10000/0.12938)
csv = open("old_parkingOptions.csv").read()
newfile = open("parkingOptions.csv", "w+")
newfile.write("Name,L_X,L_Y,cap\n")
##print csv
l = csv.split("\r\n")
##print l
for i in range(1, len(l)):
    try:
        s = l[i].split(",")
        x = float(s[1])
        y = float(s[2])
        cap = s[3]
    except:
        pass
    name = s[0]
    newx = int(fx(x))
    newy = int(fy(y))
    print name, newx, newy
    newfile.write(name+",")
    newfile.write(str(newy))
    newfile.write(",")
    newfile.write(str(newx))
    newfile.write(",")
    newfile.write(cap)
    newfile.write("\n")

newfile.close()
