'''
Marc Widmer 2018: ETHZ - Self-Organizing Multi-Agent Systems

This file scraps all the number of free spots of all parking options
in the parking guidance system in Zurich.
Additionally, it gives an update to a cell phone using a Telegram bot.
These parts have been commented out such that it can be run without
this reporting mechanism.

This script can be excetuded without any inputs or similar.
'''

import feedparser
import re
import sys
import time
import datetime
import csv
import os.path
import telegram

bot = telegram.Bot('409087977:AAHqCVSXJLvU-vGKAm4AteRzRVLS7_uDi58')
chat_id = 227965379

url = "http://www.pls-zh.ch/plsFeed/rss"
data = {'Time' : "",
'Parkgarage am Central / Seilergraben' : 0,
'Parkhaus Accu / Otto-Sch?tz-Weg' : 0,
'Parkhaus Albisriederplatz / Badenerstrasse 380' : 0,
'Parkhaus Bleicherweg / Beethovenstrasse 35' : 0,
'Parkhaus Center Eleven / Sophie-T?uber-Strasse 4' : 0,
'Parkhaus City Parking / Gessnerallee 14' : 0,
'Parkhaus Cityport / Affolternstrasse 56' : 0,
'Parkhaus Crowne Plaza / Badenerstrasse 420' : 0,
'Parkhaus Dorflinde / Schwamendingenstrasse 31' : 0,
'Parkhaus Feldegg / Riesbachstrasse 7' : 0,
'Parkhaus Globus / L?wenstrasse 50' : 0,
'Parkhaus Hardau II / Bullingerstrasse 73' : 0,
'Parkhaus Hauptbahnhof / Sihlquai 41' : 0,
'Parkhaus Hohe Promenade / R?mistrasse 22a' : 0,
'Parkhaus Jelmoli / Steinm?hleplatz 1' : 0,
'Parkhaus Jungholz / Jungholzstrasse 19' : 0,
'Parkhaus Max-Bill-Platz / Armin-Bollinger-Weg' : 0,
'Parkhaus Messe Z?rich AG / Andreasstrasse 65' : 0,
'Parkhaus Nordhaus / Siewerdtstrasse 8' : 0,
'Parkhaus Octavo / Brown-Boveri-Strasse 2' : 0,
'Parkhaus Op?ra / Schillerstrasse 5' : 0,
'Parkhaus P West / F?rrlibuckstrasse 151' : 0,
'Parkhaus Park Hyatt / Beethovenstrasse 21' : 0,
'Parkhaus Parkside / Sophie-T?uber-Strasse 10' : 0,
'Parkhaus Pfingstweid / Pfingstweidstrasse 1' : 0,
'Parkhaus Stampfenbach / Niklausstrasse 1' : 0,
'Parkhaus Talgarten / N?schelerstrasse 31' : 0,
'Parkhaus USZ Nord / Frauenklinikstrasse' : 0,
'Parkhaus Uni Irchel / Winterthurerstrasse 181' : 0,
'Parkhaus Urania / Uraniastrasse 3' : 0,
'Parkhaus Utoquai / F?rberstrasse 6' : 0,
'Parkhaus Z?ri 11 Shopping / Nansenstrasse 5/7' : 0,
'Parkhaus Z?richhorn / Dufourstrasse 142' : 0,
'Parkplatz Eisfeld / Thurgauerstrasse 54' : 0,
'Parkplatz Theater 11 / D?rfli-/Thurgauerstrasse' : 0,
'Parkplatz USZ S?d / Gloriastrasse' : 0
}

errorCounter = 10
fileExists = False
total = 0 #counts total number of used lots - used for debugging

rss = feedparser.parse(url)

while rss.status != 200 or rss.bozo:
    errorMsg = ("Error: %d" %rss.status) + " - " + str(rss.bozo)
    print errorMsg
    bot.send_message(chat_id = chat_id, text = errorMsg)
    errorCounter -= 1
    if errorCounter < 0:
        sys.exit()
    time.sleep(5)
    rss = feedparser.parse(url)

data['Time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

print rss.feed.title.encode('ascii', 'replace') + '\n' + data['Time'] + '\n'

for item in rss.entries:
    title = item.title.encode('ascii', 'replace')
    rawCount = re.findall(r'\d+', item.summary)
    count = int(rawCount[0]) if len(rawCount) > 0 else 0
    data[title] = count
    total += count
    print ('%-50s' % title) + " - " + str(count)

if os.path.isfile("parkingData.csv"):
    fileExists = True

with open('parkingData.csv', mode = 'a') as csvFile:
    writer = csv.DictWriter(csvFile, fieldnames = data.keys())
    if not fileExists:
        writer.writeheader()
    writer.writerow(data)

bot.send_message(chat_id = chat_id, text = data['Time'] + " completed - Count: " + str(total))

