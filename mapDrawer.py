'''
Marc Widmer 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script creates maps with the positions of agents and/or the parking options
indicated.

Usage:
    python mapDrawer.py [1] [2] [3]

    1. File containing a list of agents
    2. File containing the information about the parking options
    3. Time of insterest Hour:Minute

Example:
    python mapDrawer.py randAgents/rand-2018-11-16_18:30.csv parkingOptions.csv 18:30
'''

from PIL import Image, ImageDraw
import os
import sys
import csv

currDir = os.path.dirname(os.path.abspath(__file__))
agentPath = os.path.join(currDir, sys.argv[1]) #path to file of spanned agents
parkingPath = os.path.join(currDir, sys.argv[2]) #path to file of parking options
time = sys.argv[3] #time in %H:%M format

targetDir = os.path.join(currDir, 'graphicsOut')

dotRadius = 2

im = None
draw = None

imageX = 0
imageY = 0


def setup():
    if not os.path.exists(targetDir):
        try:
            os.mkdir(targetDir)
        except Exception as e:
            print("failed to create output directory - " + str(e))
            exit()


def initDraw():
    global draw, im, imageX, imageY

    try:
        im = Image.open("graphics/zurich.png")
    except Exception as e:
        print("failed to open base image - " + str(e))
    imageX = im.size[0]
    imageY = im.size[1]
    draw = ImageDraw.Draw(im)


def getImX(x):
    return float(x)/10000*imageX


def getImY(y):
    return imageY - float(y)/10000*imageY


def drawAgent(x, y):
    imX = getImX(x)
    imY = getImY(y)
    draw.ellipse([imX-dotRadius, imY-dotRadius, imX+dotRadius, imY+dotRadius], fill=(255,0,0), outline=None)


def drawParkingOption(x, y):
    imX = getImX(x)
    imY = getImY(y)
    draw.ellipse([imX-dotRadius, imY-dotRadius, imX+dotRadius, imY+dotRadius], fill=(0,255,0), outline=None)


def drawAgents():
    try:
        with open(agentPath, 'rb') as agentsFile:
            reader = csv.DictReader(agentsFile, delimiter=',')
            for row in reader:
                drawAgent(int(row['agentX']), int(row['agentY']))
    except Exception as e:
        print("failed to read or draw agents - " + str(e))
        exit()


def drawParkingOptions():
    try:
        with open(parkingPath, 'rb') as parkingFile:
            reader = csv.DictReader(parkingFile, delimiter=',')
            for row in reader:
                drawParkingOption(int(row['L_X']), int(row['L_Y']))
    except Exception as e:
        print("failed to read or draw parking options - " + str(e))
        exit()

def drawSingleParkingOptions():
    try:
        with open(parkingPath, 'rb') as parkingFile:
            reader = csv.DictReader(parkingFile, delimiter=',')
            row = next(reader)
            drawParkingOption(int(row['L_X']), int(row['L_Y']))
    except Exception as e:
        print("failed to read or draw parking options - " + str(e))
        exit()

def createGraphics():
    global draw
    initDraw()
    agentsOutPath = os.path.join(targetDir, "agents_" + time + ".png")
    drawAgents()
    del draw
    try:
        im.save(agentsOutPath, 'PNG')
    except Exception as e:
        print("failed to save agents graphic " + str(e))

    initDraw()
    parkingOutPath = os.path.join(targetDir, "parking.png")
    drawSingleParkingOptions()
    del draw
    try:
        im.save(parkingOutPath, 'PNG')
    except Exception as e:
        print("failed to save parking options graphic " + str(e))

    initDraw()
    compositeOutPath = os.path.join(targetDir, "agentsAndparking_" + time + ".png")
    drawParkingOptions()
    drawAgents()
    del draw
    try:
        im.save(compositeOutPath, 'PNG')
    except Exception as e:
        print("failed to save combined graphic " + str(e))


setup()
createGraphics()
