'''
Luis Jira 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script executes the I-EPOS algorithm with alpha, beta in [0,1] with 1/MAX increments

Usage:
    copy this file into the root of the EPOS git repository and execute:
        python EPOSgridCompute.py
'''

import os
from tempfile import mkstemp
from shutil import copy
from os import fdopen
from subprocess import call

#TODO: configfile, java command(,renaming)

dirpath = os.getcwd()
print("Base directory: "+dirpath)
confPath = dirpath+"/conf/epos.properties"
confFile = os.path.join(confPath)

MAX = 15

for i in range(0,MAX+1):
    for j in range(0,MAX-i+1):
        fh, abs_path = mkstemp()
        with fdopen(fh,'w') as newFile:
            with open(confFile) as oldFile:
                lines = oldFile.readlines()
                lines[35] = 'weightsString = "'+str(float(i)/MAX)+','+str(float(j)/MAX)+'"\n'

                newFile.writelines(lines)

        copy(abs_path, confPath)

        print(call(["/usr/bin/java","-jar",dirpath+"/target/tutorial-0.0.1.jar"]))

print('DONE')
