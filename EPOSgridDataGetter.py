'''
Luis Jira 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script collects localCost, globalCost, unfairness (all with Stddev).

Usage:
    copy this file into the root of the EPOS git repository and execute:
        python EPOSdataGetter.py
'''

import csv
import os

data = []

fieldnames = [
    'alpha',
    'beta',
    'globalCost',
    'globalCostStdev',
    'localCost',
    'localCostStdev',
    'unfairness',
    'unfairnessStdev'
]

dirpath = os.getcwd()
print("Base directory (searching for '/output' here): "+dirpath)
walked = os.walk(dirpath+"/output").next()
dirs = walked[1]
for dir in dirs:
    print("dir: "+dir)
    localCostFile = open(os.path.join(dirpath+"/output/"+dir+"/local-cost.csv"), 'rU')
    globalCostFile = open(os.path.join(dirpath+"/output/"+dir+"/global-cost.csv"), 'rU')
    unfairnessFile = open(os.path.join(dirpath+"/output/"+dir+"/unfairness.csv"), 'rU')
    confFile = open(os.path.join(dirpath+"/output/"+dir+"/used_conf.txt"), 'rU')
    temp = {}
    iterations = 24


    confLines = confFile.readlines()
    splitLines = []
    for line in confLines:
        splitLines.append(line.split(' = '))

    for line in splitLines:
        if line[0]=='numIterations':
            iterations = int(line[1])-1
        if line[0]=='alpha':
            temp['alpha'] = float(line[1])
        if line[0]=='beta':
            temp['beta'] = float(line[1])
            break

    reader = csv.DictReader(unfairnessFile, delimiter = ',')
    for i in range(0,iterations):
        reader.next()
    line = reader.next()
    temp['unfairness']=line['Mean']
    temp['unfairnessStdev']=line['Stdev']

    reader = csv.DictReader(localCostFile, delimiter = ',')
    for i in range(0,iterations):
        reader.next()
    line = reader.next()
    temp['localCost']=line['Mean']
    temp['localCostStdev']=line['Stdev']

    reader = csv.DictReader(globalCostFile, delimiter = ',')
    for i in range(0,iterations):
        reader.next()
    line = reader.next()
    temp['globalCost']=line['Mean']
    temp['globalCostStdev']=line['Stdev']

    data.append(temp)

with open(os.path.join(dirpath+"/dataOutput.csv"), 'w') as out:
    writer = csv.DictWriter(out, fieldnames)
    writer.writeheader()
    for i in range(0, len(data)):
        writer.writerow(data[i])
    print("Wrote to file: ./dataOutput.csv")
