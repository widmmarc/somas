'''
Marc Widmer 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script takes a list of agents and creates a list of plans in the
for every single one of them according to the specification in the
written part of the assignment.

Usage:
    python plansCreator.py [1] [2] [3] [4]

    1. File containing a list of agents
    2. File containing the information about the parking options
    3. File containing the scrapper data about all free spots in the PLS
    4. Time of insterest Hour:Minute

Example:
    python plansCreator.py randAgents/rand-2018-11-16_18:30.csv parkingOptions.csv 15-11-2018.csv 18:30
'''

import math
import os
import sys
import csv

PHI = 0.1
N = 8 #assuming 1 unit corresponds to 50m

currDir = os.path.dirname(os.path.abspath((__file__)))
inputAgents = os.path.join(currDir, sys.argv[1]) #file containing all spanned agents
inputParkingOptions = os.path.join(currDir, sys.argv[2]) #file containing info about all parking options
inputParkingHistory = os.path.join(currDir, sys.argv[3]) #file containing the history gathered about po's occupancy
time = sys.argv[4] #time point at which the cars are being allocated
targetDir = os.path.join(currDir, "datasets/parking_"+time)

agents = [] #array of dicts, each one containing coords of agent
options = [] #array of dicts, each one containing coords of po
occupancy = [] #info about occupancy of pos

#setting up folder structure, and reading and checking input files
def setup():
    global agents
    global options
    global occupancy

    #checking for existing and creating or clearing target directory
    if not os.path.exists(targetDir):
        try:
            os.mkdir(targetDir)
        except  Exception as e:
            print("failed to create target directory - " + str(e))
            exit()
    else:
        for file in os.listdir(targetDir):
            filePath = os.path.join(targetDir, file)
            try:
                if os.path.isfile(filePath):
                    os.unlink(filePath)
            except  Exception as e:
                print("failed to delete file - " + file + " - " + str(e))
                exit()

    #reading agents input data
    try:
        with open(inputAgents, 'rb') as agentsFile:
            reader = csv.DictReader(agentsFile, delimiter = ',')
            for row in reader:
                agents.append({'x': int(row['agentX']), 'y': int(row['agentY']), 'lambda': float(row['agentLAMBDA'])})
    except  Exception as e:
        print("failed to read agents input file - " + str(e))
        exit()

    #reading parking options input data
    try:
        with open(inputParkingOptions, 'rb') as optionsFile:
            reader = csv.DictReader(optionsFile, delimiter = ',')
            for row in reader:
                options.append({'x': int(row['L_X']), 'y': int(row['L_Y']), 'c': int(row['cap'])})
    except  Exception as e:
        print("failed to read parking options input file - " + str(e))
        exit()

    #reading relevant occupancy info (actually num of free spots)
    try:
        with open(inputParkingHistory, 'rb') as historyFile:
            reader = csv.reader(historyFile, delimiter = ',')
            first = True
            found = False
            for row in reader:
                if first:
                    first = False
                    continue
                if time in row[0]:
                    found = True
                    for i in range(1, len(row)-1):
                        takenSpots = options[i-1]['c'] - int(row[i])
                        if takenSpots < 0:
                            print("error in data - parking option " + str(i-1) + " has more free spots than available spots")
                            exit()
                        occupancy.append(takenSpots)
                    break
            if not found:
                print("count find history entry at time: " + str(time))
                exit()
    except Exception as e:
        print("failed to read occupancy info - " + str(e))

def calcLocalCost(a, o):
    dist = math.sqrt((a['x']-o['x'])**2 + (a['y']-o['y'])**2)
    if dist == 0:
        return 0
    pureCost = math.log(dist/N)
    return PHI*pureCost

def createPlans(a):
    try:
        with open(os.path.join(targetDir, "agent_" + str(a) + ".plans"), 'a') as plans:
            for s in range(0, len(options)):
                o = options[s]
                cost = calcLocalCost(agents[a], o)
                plans.write(str(cost) + ":")
                #plans.write('0.2:')
                for i in range(0, len(options)-1):
                    if i == s:
                        plans.write(str(float(1)/options[i]['c']) + ",")
                        #plans.write('0.5,')
                    else:
                        plans.write("0,")


                if s == len(options)-1:
                    plans.write(str(float(1)/options[-1]['c']) + '\n')
                    #plans.write('0.5')
                else:
                    plans.write("0\n")

    except Exception as e:
        print("failed to create plans for agent " + str(a) + " - " + str(e))
        exit()

def createFinalAgentPlan(a):
    try:
        with open(os.path.join(targetDir, "agent_" + str(a) + ".plans"), 'a') as plans:
            for s in range(0, len(options)):
                plans.write('0:')
                for i in range(0, len(options)-1):
                    plans.write(str(float(occupancy[i])/options[i]['c']) + ",")
                plans.write(str(float(occupancy[-1])/options[-1]['c']) + '\n')

            '''
            for s in range(0, len(options)):
                plans.write("0:")
                for i in range(0, len(options)-1):
                    plans.write(str(float(occupancy[i])/options[i]['c']) + ",")
                    #plans.write('0.5,')
                plans.write(str(float(occupancy[-1])/options[-1]['c']) + "\n")
                #plans.write('0.5')
            '''
    except Exception as e:
        print("failed to create final agent plan: " + str(a) + " - " + str(e))

def createTarget():
    totalSpotsNeeded = len(agents)
    for o in occupancy:
        totalSpotsNeeded += o

    totalSpots = 0
    for o in options:
        totalSpots += o['c']

    target = float(totalSpotsNeeded)/totalSpots

    try:
        with open(os.path.join(targetDir, "minVar.target"), 'a') as t:
            t.write((str(target) + ", ")*(len(options)-1))
            t.write(str(target))
                    
    except Exception as e:
        print("failed to create target: " + str(e))
        exit()

counter = 0
setup()
while counter < len(agents):
    createPlans(counter)
    counter += 1
createFinalAgentPlan(counter)
#createTarget()
