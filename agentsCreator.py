'''
Luis Jira 2018: ETHZ - Self-Organizing Multi-Agent Systems

This script creates a list of agents based on the approximated demand within a specified interval.

Usage:
    python agentsCreator.py [1] [2] [3] [4]

    1. File containing the scrapper data about all free spots in the PLS
    2. File containing information about all parking options
    3. Date of interest Year-Month-Day
    4. Starting time of interval of interest Hour-Minute

Example:
    pyhton agentsCreator.py 15-11-2018.csv parkingOptions.csv 2018-11-15 18:30
'''

import csv
import sys
import time
import datetime
import os
import
from operator import itemgetter

targetDirName = "/randAgentPositions"
currDir = os.path.dirname(os.path.realpath(__file__))
targetDir = os.path.join(currDir+targetDirName)
inputParkingHistory = os.path.join(currDir+"/"+sys.argv[1]) #file containing the history gathered about po's occupancy
inputCoordinates = os.path.join(currDir+"/"+sys.argv[2]) #file containing info about all parking options
timeKey = 'Time'
timeVars = [timeKey, 'timeDiff']
try:
    timeArg = sys.argv[3]+" "+sys.argv[4] #time point at which the cars are being allocated
except:
    timeArg = False

MIN_DIST = 100 /5 #value in meters / scale
MAX_DIST = 2000 /5 #value in meters / scale
MIN_LAMBDA = 0.1
MAX_LAMBDA = 0.9

timepoints = [] #array of dicts, each one containing amount of entries per timepoint
coordinates = {} #Dict, containing coords of POs and capacity
agents = [] #array of dicts, each one containing an agent, with x,y coordinates and lambda (maybe time)

def setup():
    "setting up folder structure, and reading and checking input files"
    #checking for existing and creating or clearing target directory
    if not os.path.exists(targetDir):
        os.mkdir(targetDir)


    #reading parking coordinates
    with open(inputCoordinates, 'rU') as parkingFile:
        reader = csv.DictReader(parkingFile, delimiter = ',')
        for row in reader:
            coordinates[row['Name']]= (row['L_X'], row['L_Y'])

    #reading number of changed spots
    with open(inputParkingHistory, 'rb') as historyFile:
        reader = csv.DictReader(historyFile, delimiter = ',')
        first = True
        found = False
        temp = {}
        if timeArg:
            for row in reader:
                if(timeArg in row.get(timeKey)):
                    if temp:
                        found = True
                        for key in row:
                            if(key == timeKey):
                                temp[key] = time.strptime(row[key][0:-3], "%Y-%m-%d %H:%M")
                                temp['timeDiff'] = time.mktime(temp[key])-time.mktime(smallTemp)
                                smallTemp = temp[key]
                                continue
                            temp[key] = int(temp[key]) - int(row[key])
                            if int(temp[key]) < 0:
                                temp[key] = 0
                        timepoints.append(temp)
                        break
                    else:
                        print("error: you chose first logged time")
                        exit()
                else:
                    smallTemp = time.strptime(row[timeKey][0:-3], "%Y-%m-%d %H:%M")
                temp = row
        else:
            for row in reader:
                if temp:
                    found = True
                    for key in row:
                        if(key == timeKey):
                            temp[key] = time.strptime(row[key][0:-3], "%Y-%m-%d %H:%M")
                            temp['timeDiff'] = time.mktime(temp[key])-time.mktime(smallTemp)
                            smallTemp = temp[key]
                            continue
                        temp[key] = int(temp[key]) - int(row[key])
                        if int(temp[key]) < 0:
                            temp[key] = 0
                    timepoints.append(temp)
                else:
                    smallTemp = time.strptime(row[timeKey][0:-3], "%Y-%m-%d %H:%M")
                temp = row
        if not found:
            print("couldn't find history entry at time: " + timeArg)
            exit()
    return;

#TODO complex function
def randPos(x_PO, y_PO, ID, sliceStartTime, spawnInterval, spawnID):
    "Generates  agent spawning position depending on input coordinates"
    x_PO = int(x_PO)
    y_PO = int(y_PO)
    agentTime = time.strftime("%Y-%m-%d %H:%M", time.gmtime(time.mktime(sliceStartTime)+(spawnInterval*spawnID)))
    rand = .()
    randSign = .randrange(0,2)
    agentX = int(-1*randSign*(rand*(MAX_DIST-MIN_DIST)+MIN_DIST)+x_PO)
    rand = .()
    randSign = .randrange(0,2)
    agentY = int(-1*randSign*(rand*(MAX_DIST-MIN_DIST)+MIN_DIST)+y_PO)
    agentLAMBDA = .()
    while agentLAMBDA > MAX_LAMBDA or agentLAMBDA < MIN_LAMBDA:
        agentLAMBDA = .()
    return {'ID':ID, 'agentTime':0, 'agentX':agentX, 'agentY':agentY, 'agentLAMBDA':agentLAMBDA};

def write():
    "Writes agents Dictionary to file"
    if timeArg:
        fileName = "rand-"+("_".join(timeArg.split(" ")))
    else:
        split = sys.argv[1].split(".")
        fileName = "rand-"+split[0]
    if os.path.isfile(targetDir+"/"+fileName+".csv"):
        i = 0
        while(os.path.isfile(targetDir+"/"+str(i)+"_"+fileName+".csv")):
            i += 1
        fileName = str(i)+"_"+fileName
    fileName = fileName+".csv"
    with open(os.path.join(targetDir+"/"+fileName), 'a') as out:
        writer = csv.DictWriter(out, fieldnames = ['ID', 'agentTime', 'agentX', 'agentY', 'agentLAMBDA'])
        writer.writeheader()
        for i in range(0, len(agents)):
            writer.writerow(agents[i])
        print("Wrote to file: "+targetDirName+"/"+fileName)
    return;

def sort(dictArray):
    dictArray = sorted(dictArray, key=itemgetter('agentTime'))
    for i in range(0,len(dictArray)):
        dictArray[i]['ID'] = i
    return dictArray;

setup()
agentId = 1;
spawnInterval = 60
for timePoint in timepoints:
    for option in timePoint:
        if option in timeVars:
            continue;
        if int(timePoint[option]) != 0:
            spawnInterval = timePoint['timeDiff']/timePoint[option]
        for i in range(1, int(timePoint[option])):
            randDict = randPos(coordinates[option][0], coordinates[option][1], agentId, timePoint[timeKey], spawnInterval, i)
            agents.append(randDict)
            agentId += 1
agents = sort(agents)
write()
